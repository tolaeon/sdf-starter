define(['N/record', 'N/search', 'N/plugin', 'N/format', 'N/error'], function (record, search, plugin, format, error) {
    /**
     * @fileOverview This is a restlet stub
     * @copyright 2019 Tolaeon
     * @author Brendan Boyd <bboyd@tolaeon.io>
     * @version 0.1.0
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType RESTlet
     */
    var exports = {};


    /**
     * <code>get</code> event handler
     * @governance XXX
     * @param params
     *        {Object} The parameters from the HTTP request URL as key-value pairs;
     * @param params.processorId
     *        {Number} The GUID of the data queue record.
     * @return {String|Object} Returns a String when request
     *         <code>Content-Type</code> is <code>text/plain</code>;
     *         returns an Object when request <code>Content-Type</code> is
     *         <code>application/json</code>;
     *         Representation is based on the requested data queue type.
     * @static
     * @function get
     */
    function _get(params) {
        throw 'Get not supported';
    }

    /**
     * <code>post</code> event handler
     * @governance XXX
     * @param request
     *        {String|Object} The request body as a String when
     *            <code>Content-Type</code> is <code>text/plain</code>; The
     *            request body as an Object when request
     *            <code>Content-Type</code> is <code>application/json</code>
     * @param request.processorId
     * @param request.type
     * @return {String|Object} Returns a String when request
     *         <code>Content-Type</code> is <code>text/plain</code>;
     *         returns an Object when request <code>Content-Type</code> is
     *         <code>application/json</code>
     * @static
     * @function post
     */
    function post(request) {
        return "success";
    }

    /**
     * <code>put</code> event handler
     * @governance XXX
     * @param request
     *        {String|Object} The request body as a String when
     *            <code>Content-Type</code> is <code>text/plain</code>; The
     *            request body as an Object when request
     *            <code>Content-Type</code> is <code>application/json</code>
     * @return {String|Object} Returns a String when request
     *         <code>Content-Type</code> is <code>text/plain</code>;
     *         returns an Object when request <code>Content-Type</code> is
     *         <code>application/json</code>
     * @static
     * @function put
     */
    function put(request) {
        throw 'Put not supported';
    }

    /**
     * <code>delete</code> event handler
     * @governance XXX
     * @param params
     *        {Object} The parameters from the HTTP request URL as key-value pairs
     * @return {String|Object} Returns a String when request
     *         <code>Content-Type</code> is <code>text/plain</code>;
     *         returns an Object when request <code>Content-Type</code> is
     *         <code>application/json</code>
     * @static
     * @function _delete
     */
    function _delete(params) {
        throw 'Delete not supported';
    }

    exports.get = _get;
    exports.post = post;
    exports.put = put;
    exports.delete = _delete;
    return exports;
});
